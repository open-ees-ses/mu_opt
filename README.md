# Multi-use optimization tool for energy storage systems (mu_opt)
This stand-alone tool generates the optimized operating strategy for energy storage systems. Due to its flexible structure, this optimization framework enables the simultaneous use of multiple storage applications (dynamic multi-use).

The tool was created by Stefan Englberger at the Institute for Electrical Energy Storage Technology at the Technical University of Munich. For efficient optimization, the tool was designed in Mathworks Matlab and also supports the Gurobi solver.

### **Highlights & Features:**

#### Maximization of profit from multi-use operation
- Dynamic allocation of energy and power of physical energy storage system
- Segmentation into virtual energy and power partitions
- Behind-the-meter and front-the-meter applications

#### Optimization
- Mixed-integer linear programming
- Problem-based optimization framework
- Linearization of non-linear internal processes

#### Supporting state-of-the-art applications
- Frequency containment reserve
- Peak shaving
- Self-consumption improvement
- Spot market trading

#### Technical considerations include
- Component efficiencies and system peripherals
- Degradation model for well-established cell chemistry
- Self-discharge of storage system

#### Regulatory constraints include
- System topology
- Application's degrees of freedom
- Spot market regulations

### **How to cite:**

[Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).](https://doi.org/10.1016/j.xcrp.2020.100238)
