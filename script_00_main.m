%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

%% clear workspace
	clc; clear; close all;

%% run optimization
try
	Input.scenario_folder = '';
	Input.scenario_name = datestr(now,'yyyymmddHHMMSS');
	disp(['Simulation ',Input.scenario_folder,Input.scenario_name,' started.']);
		run('script_10_initialization.m'); % Data initialization
		run('script_20_optimization.m'); % Run optimization
		run('script_30_post_calculation.m'); % Concatenate and save files
	disp(['Simulation ',Input.scenario_folder,Input.scenario_name,' was successful.']);
catch
	try config_step = uint32(optimization_loop); catch; config_step = 0; end
	try config_steps = uint32(Input.simulation_time/Input.rolling_horizon); catch; config_steps = 0; end
	disp(['WARNING: Simulation ',Input.scenario_folder,Input.scenario_name,' was aborted at [',num2str(config_step),' / ',num2str(config_steps),'].']);
end

%% clear workspace
	clearvars -except Input* Optimization*;