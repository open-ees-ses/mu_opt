%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

% start timer
	tic;

%% general settings
	Input.sample_time					= 5 / 60; % [h] length of sample time
	Input.optimization_time				= 24; % [h] length of optimization period
	Input.rolling_horizon				= 2; % [h] length of rolling horizon period
	Input.simulation_time				= 365 * 24; % [h] length of simulation period
	Input.simulation_start				= 0 * 24; % [h] start time of simulation (effects profiles)

	path_profile						= 'profiles.mat'; % path of profiles
	path_results						= 'results\'; % path of results folder
	Input.intermediate_data				= 0; % save intermediate results all x optimizatinon loops
	Input.optimization_full				= false; % save full optimization data
	Input.soh_caluclation				= true; % state of health calculation

% rolling horizon has to be a multiple of the sample time
	if mod(Input.rolling_horizon,Input.sample_time) ~= 0
		error('Rolling horizon must be a multiple of the sample time.');
	end

% optimization time has to be a multiple of the sample time
	if mod(Input.optimization_time,Input.sample_time) ~= 0
		error('Optimization time must be a multiple of the sample time.');
	end

% simulation time has to be a multiple of the rolling horizon
	if mod(Input.simulation_time,Input.rolling_horizon) ~= 0
		Input.simulation_time = Input.rolling_horizon * ceil(Input.simulation_time/Input.rolling_horizon);
		warning(['Simulation time must be a multiple of the rolling horizon period.',newline,' Simulation time corrected to ',num2str(Input.simulation_time/24),' days.']);
	end

% enable/disable behind-the-meter (btm) and front-the-meter (ftm) applications
	Input.fcr							= true;	% conduct frequency containment reserve (fcr, ftm)
	Input.ps							= true; % conduct peak shaving (ps, btm)
	Input.qc							= true; % conduct reactive power compensation
	Input.sci							= true; % conduct self-consumption increase (sci, btm)
	Input.smt							= true; % conduct spot market trading (smt, ftm)

% define the input folder and profiles
	Input.number_price_fcr				= 13; % number of profile for fcr price
	Input.type_price_smt				= 'idc'; % type of spot market ('dam' or 'idc')
	Input.number_price_smt_low			= 37; % number of profile for spot market price (low price signal)
	Input.number_price_smt_high			= 38; % number of profile for spot market price (high price signal)
	Input.number_power_generator		= 5; % number of profile for active power from generator
	Input.number_power_load_active		= 212; % number of profile for active load power
	Input.number_power_load_reactive	= 1; % number of profile for reactive load power
	Input.number_frequency				= 8; % number of profile for frequency

% optimization options
	Input.max_optimization_time			= 900; % [s] maximum time per optimization
	Input.max_relative_optimization_gap	= 5e-3; % [1] maximum relative gap per optimization

%% energy storage system (ess)
	Input.ESS.state_of_health_initial	= 1.0; % [1] initial state of health of storage system
	Input.ESS.energy_nominal			= 1340; % [kWh] nominal energy content of storage system
	Input.ESS.inverter_amount			= 24; % integer amount of inverters
	Input.ESS.power_apparent			= 65.5; % [kVA] rated apparent power per inverter
	Input.ESS.power_active				= 0.8 * Input.ESS.power_apparent; % [kW] rated active power per inverter
	Input.ESS.power_reactive			= 0.3 * Input.ESS.power_apparent; % [kvar] rated reactive power per inverter
	Input.ESS.state_of_charge_min		= 0.00; % [1] minimum state of charge (lower bound)
	Input.ESS.state_of_charge_max		= 1.00; % [1] maximum state of charge (upper bound)
	Input.ESS.state_of_charge_initial	= 0.50; % [1] state of charge at the begin of simulation
	Input.ESS.state_of_charge_end		= 0.50; % [1] state of charge at the end of simulation
	Input.ESS.efficiency_battery		= 0.97; % [1] efficiency of battery cells
	Input.ESS.efficiency_inverter		= 0.96; % [1] efficiency of power electronics
	Input.ESS.efficiency_transformer	= 1.00; % [1] efficiency of transformer
	Input.ESS.efficiency_charge			= prod([Input.ESS.efficiency_battery,Input.ESS.efficiency_inverter,Input.ESS.efficiency_transformer]); % [1] efficiency during charging
	Input.ESS.efficiency_discharge		= prod([Input.ESS.efficiency_battery,Input.ESS.efficiency_inverter,Input.ESS.efficiency_transformer]); % [1] efficiency during discharging
	Input.ESS.power_active_charge		= min(1.00*Input.ESS.energy_nominal/1,Input.ESS.inverter_amount*Input.ESS.power_active); % [kW] maximum active power during charging of ESS
	Input.ESS.power_active_discharge	= min(1.00*Input.ESS.energy_nominal/1,Input.ESS.inverter_amount*Input.ESS.power_active); % [kW] maximum active power during discharging of ESS
	Input.ESS.allocation_switching_time	= 1.0; % [h] switching time of power and energy allocation
	Input.ESS.temperature_cell			= 20 + 273.15; % [K] cell temperature
	Input.ESS.capacity_cell_nominal		= 2.05; % [Ah] nominal cell capacity
	Input.ESS.energy_self_discharge		= 0.015/(30.5*24/Input.sample_time) * Input.ESS.energy_nominal; % self-discharge depending on nominal energy content
	Input.ESS.energy_shift				= [-0.0 , 0.0] * Input.ESS.energy_nominal; % maximum energy exchange between ftm and btm partition
	Input.ESS.support_power				= 0.00 * Input.ESS.energy_nominal; % [kW] additional power consumption for support energy of storage system
	Input.ESS.invest_system				= 380 * Input.ESS.energy_nominal; % [EUR] invest costs for storage system
	Input.ESS.efc_capability			= 3000; % [1] equivalent full cycles capability until system's end of life

%% price signals
	% https://www.bdew.de/service/daten-und-grafiken/strompreis-fuer-die-industrie/
	Input.price_base_distribution		= 0.04300; % [EUR/kWh] base price for electricity distribution
	Input.price_power_charges			= 100.0; % [EUR/kW] power related grid charges
	Input.price_grid_charges			= 0.00800; % [EUR/kWh] energy related grid charges
	Input.price_concession_fee			= 0.00110; % [EUR/kWh] concession fee in Germany (http://www.gesetze-im-internet.de/kav/__2.html)
	Input.price_eeg_surcharge			= 0.06405; % [EUR/kWh] eeg surcharge in Germany (https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Verbraucher/PreiseRechnTarife/preiseundRechnungen-node.html)
	Input.price_kwkg_surcharge			= 0.00280; % [EUR/kWh] kwkg surcharge in Germany (http://www.gesetze-im-internet.de/kwkg_2016/)
	Input.price_strom_nev_surcharge		= 0.00200; % [EUR/kWh] strom nev surcharge (paragraph 19 Strom NEV) in Germany (https://www.gesetze-im-internet.de/stromnev/__19.html)
	Input.price_offsu_surcharge			= 0.00416; % [EUR/kWh] offshore liability levy in Germany (https://www.gesetze-im-internet.de/enwg_2005/__17f.html)
	Input.price_ablav_surcharge			= 0.00005; % [EUR/kWh] surcharge for interruptible loads in Germany (Abschaltverordnung AbLaV) (https://www.gesetze-im-internet.de/ablav_2016/__18.html)
	Input.price_electricity_tax			= 0.01537; % [EUR/kWh] electricity tax in Germany
	Input.price_trading_fee				= 0.0001425; % [EUR/kWh] electricity trading fee

%% import profiles and application-specific data
% import profiles
	Input.peak_generation				= 0; % [kW] peak power of generation profile
	Input.consumption					= 20e6; % [kWh] annual energy consumption
	Input.curtailment_btm_max			= Inf; % [kW] maximum curtailment power at btm partition

% front-the-meter (ftm) settings
	Input.curtailment_ftm_max			= 0; % [kW] maximum curtailment power at ftm partition
	run('script_11_load_profiles.m'); % load existing profiles
	run('script_12_create_profiles.m'); % create necessary profiles

% frequency containment reserve (fcr) settings
	Input.FCR.objective_weight			= 1; % [1] relative weighting of the application in the objective function
	Input.FCR.period					= 4; % [h] length of minimum period for fcr provision (must be multiple of sample time)
	Input.FCR.minimum_power				= 100; % [kW] minimum power for fcr provision
	Input.FCR.reserve_time				= 15/60; % [h] reserved time for additional energy criteria
	Input.FCR.reserve_power				= 0.25; % [1] reserved power for additional power criteria
	Input.FCR.frequency_nominal			= 50.0; % [Hz] nominal frequency
	Input.FCR.frequency_deadband		= 0.01; % [Hz] frequency dead band of fcr provision
	Input.FCR.frequency_droop			= 0.4/100; % [1] frequency droop
	Input.FCR.overfulfillment			= 1.2; % [1] maximum overfulfillment
	Input.FCR.energy_difference			= 1.0 * Input.ESS.energy_nominal; % [kWh] maximum energy difference at start and end of provision period
	Input.FCR.revenue_share_min			= 0.60; % [1] minimum share of revenue from fcr provision
	Input.FCR.revenue_share_max			= 0.85; % [1] maximum share of revenue from fcr provision
	Input.FCR.revenue_share_max_power	= 1000; % [kW] power threshold to reach maximum share of revenue
	run('script_12_create_profiles.m'); % create necessary profiles
	run('script_13_fcr_bounds.m'); % create lower and upper bound of fcr power considering the specified degrees of freedom

% peak shaving (ps) settings
	Input.PS.objective_weight			= 1; % [1] relative weighting of the application in the objective function
	Input.PS.price						= Input.price_power_charges; % [EUR/kW] electricity tariff for peak power
	Input.PS.billing_period				= 365 * 24; % [h] billing period for peak power threshold
	Input.PS.power_threshold_initial	= Input.consumption / 8760; % [kW] initial peak shave threshold (for each billing period)
	Input.PS.reset_threshold			= true; % reset power threshold to initial value for each billing period

% self-consumption increase (sci) settings
	Input.SCI.objective_weight			= 1; % [1] relative weighting of the application in the objective function
	Input.SCI.grid_tariff				= sum([Input.price_base_distribution,Input.price_grid_charges,Input.price_electricity_tax,...
											Input.price_eeg_surcharge,Input.price_kwkg_surcharge,Input.price_concession_fee,...
											Input.price_strom_nev_surcharge,Input.price_offsu_surcharge,Input.price_ablav_surcharge]); % [EUR/kWh] electricity tariff for energy purchase
	Input.SCI.grid_remuneration			= 0.10610; % [EUR/kWh] remuneration price for energy sale
	Input.SCI.price_selfconsumed		= 0.4 * Input.price_eeg_surcharge; % [EUR/kWh] costs for self-consumed electricity
	Input.SCI.feedin_limit				= 1e3; % [kW] feed-in limitation for power flow to grid

% spot market trading (smt) settings
	Input.SMT.objective_weight			= 1; % [1] relative weighting of the application in the objective function
	Input.SMT.period					= 15/60; % [h] length of minimum period (must be multiple of sample time)
	Input.SMT.minimum_power				= 100; % [kW] minimum power for transaction
	Input.SMT.charges_purchase			= Input.price_trading_fee + Input.price_electricity_tax + Input.price_concession_fee; % [EUR/kWh] charges for purchased energy
	Input.SMT.charges_sell				= Input.price_trading_fee; % [EUR/kWh] charges for sold energy
	run('script_12_create_profiles.m'); % create necessary profiles

%% check application-related errors
% rolling horizon has to be a multiple of the spot market provision period
	if mod(Input.rolling_horizon,Input.SMT.period) ~= 0
		error('Rolling horizon must be a multiple of the SMT provision period.');
	end

% optimization time has to be a multiple of the allocation switching time
	if mod(Input.optimization_time,Input.ESS.allocation_switching_time) ~= 0
		error('Optimization time must be a multiple of the allocation switching time.');
	end

% optimization time has to be a multiple of the fcr provision period
	if mod(Input.optimization_time,Input.FCR.period) ~= 0
		error('Optimization time must be a multiple of the FCR provision period.');
	end

% optimization time has to be a multiple of the spot market provision period
	if mod(Input.optimization_time,Input.SMT.period) ~= 0
		error('Optimization time must be a multiple of the SMT provision period.');
	end

% allocation switching time has to be a multiple of the sample time
	if mod(Input.ESS.allocation_switching_time,Input.sample_time) ~= 0
		error('Allocation switching time must be a multiple of the sample time.');
	end

% fcr provision period has to be a multiple of the sample time
	if mod(Input.FCR.period,Input.sample_time) ~= 0
		error('FCR provision period must be a multiple of the sample time.');
	end

% smt provision period has to be a multiple of the sample time
	if mod(Input.FCR.period,Input.sample_time) ~= 0
		error('Spot market provision period must be a multiple of the sample time.');
	end