%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

% define the length of profiles
	profile_length = ( Input.simulation_time + Input.optimization_time - Input.rolling_horizon ) / Input.sample_time;

% load profiles file if available
if exist(path_profile,'file') == 2
	load(path_profile, ...
		['profile_price_fcr_',sprintf('%03d',Input.number_price_fcr)], ...
		['profile_price_',Input.type_price_smt,'_',sprintf('%03d',Input.number_price_smt_low)], ...
		['profile_price_',Input.type_price_smt,'_',sprintf('%03d',Input.number_price_smt_high)], ...
		['profile_power_gen_',sprintf('%03d',Input.number_power_generator)], ...
		['profile_power_active_load_',sprintf('%03d',Input.number_power_load_active)], ...
		['profile_power_reactive_load_',sprintf('%03d',Input.number_power_load_reactive)], ...
		['profile_frequency_',sprintf('%03d',Input.number_frequency)] ...
	);

% fcr price signal [EUR/kWh]
	profile_price_fcr					= eval(['profile_price_fcr_',sprintf('%03d',Input.number_price_fcr),';']);
	Input.price_fcr						= repelem(double(profile_price_fcr),60,1); % [EUR/kWh] % call fcr price profile
	Input.price_fcr						= repmat(Input.price_fcr,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
	dummy								= zeros(profile_length,1);
	for i = uint32(1:profile_length); dummy(i) = mean(Input.price_fcr(Input.simulation_start*(60*60)+(i-1)*(Input.sample_time*(60*60))+1:Input.simulation_start*(60*60)+i*(Input.sample_time*(60*60)))); end; clear i;
	Input.price_fcr						= dummy; clear dummy;

% low spot market price signal [EUR/kWh]
	profile_price_smt_low				= eval(['profile_price_',Input.type_price_smt,'_',sprintf('%03d',Input.number_price_smt_low),';']);
	Input.price_smt_low					= repelem(double(profile_price_smt_low),60,1); % [EUR/kWh] % call spot market price profile
	Input.price_smt_low					= repmat(Input.price_smt_low,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
	dummy								= zeros(profile_length,1);
	for i = uint32(1:profile_length); dummy(i) = mean(Input.price_smt_low(Input.simulation_start*(60*60)+(i-1)*(Input.sample_time*(60*60))+1:Input.simulation_start*(60*60)+i*(Input.sample_time*(60*60)))); end; clear i;
	Input.price_smt_low					= dummy; clear dummy;

% high spot market price signal [EUR/kWh]
	profile_price_smt_high				= eval(['profile_price_',Input.type_price_smt,'_',sprintf('%03d',Input.number_price_smt_high),';']);
	Input.price_smt_high				= repelem(double(profile_price_smt_high),60,1); % [EUR/kWh] % call spot market price profile
	Input.price_smt_high				= repmat(Input.price_smt_high,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
	dummy								= zeros(profile_length,1);
	for i = uint32(1:profile_length); dummy(i) = mean(Input.price_smt_high(Input.simulation_start*(60*60)+(i-1)*(Input.sample_time*(60*60))+1:Input.simulation_start*(60*60)+i*(Input.sample_time*(60*60)))); end; clear i;
	Input.price_smt_high				= dummy; clear dummy;

% correct spot market price signal if smt is disabled (but scheduled transactions during fcr are enabled)
	if ~Input.smt
		Input.price_smt_low(:) = mean(mean([Input.price_smt_low,Input.price_smt_high],2));
		Input.price_smt_high = Input.price_smt_low;
	end

% active power supply [kW]
	profile_power_gen					= eval(['profile_power_gen_',sprintf('%03d',Input.number_power_generator),';']);
	Input.power_generation				= repelem(double(profile_power_gen),60,1); % call generation profile
	Input.power_generation				= Input.power_generation / max(Input.power_generation) * Input.peak_generation; % scaling of profile
	Input.power_generation				= repmat(Input.power_generation,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
	dummy								= zeros(profile_length,1);
	for i = uint32(1:profile_length); dummy(i) = mean(Input.power_generation(Input.simulation_start*(60*60)+(i-1)*(Input.sample_time*(60*60))+1:Input.simulation_start*(60*60)+i*(Input.sample_time*(60*60)))); end; clear i;
	Input.power_generation				= dummy; clear dummy;

% active power demand [kW]
	profile_power_active_load			= eval(['profile_power_active_load_',sprintf('%03d',Input.number_power_load_active),';']);
	Input.power_active_load				= repelem(double(profile_power_active_load),60,1); % call load power profile
	Input.power_active_load				= Input.power_active_load / sum(Input.power_active_load) * Input.consumption * 3.6e3; % scaling of profile
	Input.power_active_load				= repmat(Input.power_active_load,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
	dummy								= zeros(profile_length,1);
	for i = uint32(1:profile_length); dummy(i) = mean(Input.power_active_load(Input.simulation_start*(60*60)+(i-1)*(Input.sample_time*(60*60))+1:Input.simulation_start*(60*60)+i*(Input.sample_time*(60*60)))); end; clear i;
	Input.power_active_load				= dummy; clear dummy;

% reactive power [kvar]
	profile_power_reactive_load			= eval(['profile_power_reactive_load_',sprintf('%03d',Input.number_power_load_reactive),';']);
	Input.power_reactive_load			= repelem(double(profile_power_reactive_load),60,1); % call load power profile
	Input.power_reactive_load			= Input.power_reactive_load / sum(Input.power_reactive_load) * Input.consumption / 6; % scaling of profile; 6 = ratio between active and reactive profile (in this particular case)
	Input.power_reactive_load			= repmat(Input.power_reactive_load,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
	dummy								= zeros(profile_length,1);
	for i = uint32(1:profile_length); dummy(i) = mean(Input.power_reactive_load(Input.simulation_start*(60*60)+(i-1)*(Input.sample_time*(60*60))+1:Input.simulation_start*(60*60)+i*(Input.sample_time*(60*60)))); end; clear i;
	Input.power_reactive_load			= dummy; clear dummy;

% frequency signal [Hz]
	profile_frequency					= eval(['profile_frequency_',sprintf('%03d',Input.number_frequency),';']);
	Input.frequency						= double(profile_frequency); % [Hz] % call frequency profile
	Input.frequency						= repmat(Input.frequency,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1); % [Hz] % call frequency profile
	dummy								= zeros(profile_length,1);
	for i = uint32(1:profile_length); dummy(i) = mean(Input.frequency(Input.simulation_start*(60*60)+(i-1)*(Input.sample_time*(60*60))+1:Input.simulation_start*(60*60)+i*(Input.sample_time*(60*60)))); end; clear i;
	Input.frequency						= dummy; clear dummy;
end

% clear unused variables
	clear profile_* path_profile;