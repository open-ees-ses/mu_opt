%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

% define the length of profiles
	profile_length = ( Input.simulation_time + Input.optimization_time - Input.rolling_horizon ) / Input.sample_time;

% fcr price signal [EUR/kWh]
	if isfield(Input,'price_fcr') == 0 && isfield(Input,'FCR') == 1 && isfield(Input.FCR,'period') == 1
		dummy = repelem(smoothdata(normrnd(0.012,0.003,[ceil(profile_length*Input.sample_time/Input.FCR.period),1]),'gaussian',1/Input.FCR.period*12),Input.FCR.period/Input.sample_time,1);
		Input.price_fcr = dummy(mod(Input.simulation_start,Input.FCR.period)/Input.sample_time+1:mod(Input.simulation_start,Input.FCR.period)/Input.sample_time+profile_length);
		clear dummy;
	end

% spot market price signals [EUR/kWh]
	if (isfield(Input,'price_smt_low') == 0 || isfield(Input,'price_smt_high') == 0) && isfield(Input,'SMT') == 1 && isfield(Input.SMT,'period') == 1
		dummy = repelem(smoothdata(normrnd(0.045,0.03,[ceil(profile_length*Input.sample_time/Input.SMT.period),1]),'gaussian',Input.SMT.period*12),Input.SMT.period/Input.sample_time,1);
		Input.price_smt_low = dummy(mod(Input.simulation_start,Input.SMT.period)/Input.sample_time+1:mod(Input.simulation_start,Input.SMT.period)/Input.sample_time+profile_length);
		if Input.number_price_smt_low == Input.number_price_smt_high
			Input.price_smt_high = Input.price_smt_low;
		else
			dummy = repelem(smoothdata(normrnd(0.045,0.03,[ceil(profile_length*Input.sample_time/Input.SMT.period),1]),'gaussian',Input.SMT.period*12),Input.SMT.period/Input.sample_time,1);
			Input.price_smt_high = dummy(mod(Input.simulation_start,Input.SMT.period)/Input.sample_time+1:mod(Input.simulation_start,Input.SMT.period)/Input.sample_time+profile_length);
			clear dummy;
			dummy =  [Input.price_smt_low,Input.price_smt_high];
			Input.price_smt_low = min(dummy,[],2);
			Input.price_smt_high = max(dummy,[],2);
			clear dummy;
		end
		% correct spot market price signal if smt is disabled (but scheduled transactions during fcr are enabled)
		if ~Input.smt
			Input.price_smt_low(:) = mean(mean([Input.price_smt_low,Input.price_smt_high],2));
			Input.price_smt_high = Input.price_smt_low;
		end
	end

% active power supply [kW]
	if isfield(Input,'power_generation') == 0 && isfield(Input,'peak_generation') == 1
		dummy = ceil(profile_length/24*Input.sample_time)+1;
		dummy = sin(linspace(0,2*pi*dummy,dummy*24/Input.sample_time)).*abs(sin(linspace(0,2*pi*dummy,dummy*24/Input.sample_time)));
		dummy(dummy<0) = 0; dummy = circshift(dummy,6/Input.sample_time); dummy = transpose(dummy);
		dummy = (0.7+0.3*rand(size(dummy))).*dummy; dummy(dummy<0) = 0;
		dummy = dummy/max(dummy)*Input.peak_generation;
		Input.power_generation = dummy(mod(Input.simulation_start,24)/Input.sample_time+1:mod(Input.simulation_start,24)/Input.sample_time+profile_length);
		clear dummy;
	end

% active power demand [kW]
	if isfield(Input,'power_active_load') == 0 && isfield(Input,'consumption') == 1
		dummy = 0.3+0.7*rand(profile_length,1);
		dummy = movmean(dummy,6/Input.sample_time,'omitnan').^5;
		Input.power_active_load = dummy/sum(dummy)*Input.consumption*(profile_length*Input.sample_time)/(365*24)/Input.sample_time;
		clear dummy;
	end

% reactive power [kvar]
	if isfield(Input,'power_reactive_load') == 0 && isfield(Input,'consumption') == 1
		dummy = 0.3+0.7*rand(profile_length,1);
		dummy = movmean(dummy,6/Input.sample_time,'omitnan').^5;
		Input.power_reactive_load = dummy/sum(dummy)*Input.consumption/6*(profile_length*Input.sample_time)/(365*24)/Input.sample_time; % scaling of profile; 6 = ratio between active and reactive profile (in this particular case)
		clear dummy;
	end

% frequency signal [Hz]
	if isfield(Input,'frequency') == 0 && isfield(Input,'FCR') == 1 && isfield(Input.FCR,'frequency_nominal') == 1
		dummy = normrnd(Input.FCR.frequency_nominal,0.02,[profile_length,1]);
		dummy = movmean(dummy,0.25/Input.sample_time,'omitnan');
		Input.frequency = dummy;
		clear dummy;
	end

% clear unused variables
	clear profile_*;