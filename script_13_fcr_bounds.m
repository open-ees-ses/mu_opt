%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

%% calculation of fcr bounds (minimum and maximum)
% define input variables
	frequency_delta		= Input.frequency - Input.FCR.frequency_nominal; % [Hz]
	fcr_power_min		= zeros(size(Input.frequency));
	fcr_power_max		= zeros(size(Input.frequency));

% calculate power signal of fcr considering the frequency
	for i = uint32(1:length(Input.frequency))
		if frequency_delta(i) >= 0
			if abs(frequency_delta(i)) <= Input.FCR.frequency_deadband 
				fcr_power_min(i) = 0;
			else
				fcr_power_min(i) = max(-1, -(frequency_delta(i) * 1) / (Input.FCR.frequency_nominal * Input.FCR.frequency_droop));
			end
			fcr_power_max(i) = max(-1, -(frequency_delta(i) * 1) / (Input.FCR.frequency_nominal * Input.FCR.frequency_droop)) * Input.FCR.overfulfillment; 
		else
			if abs(frequency_delta(i)) <= Input.FCR.frequency_deadband 
				fcr_power_min(i) = 0;
			else
				fcr_power_min(i) = min(1, -(frequency_delta(i) * 1) / (Input.FCR.frequency_nominal * Input.FCR.frequency_droop));
			end
			fcr_power_max(i) = min(1, -(frequency_delta(i) * 1) / (Input.FCR.frequency_nominal * Input.FCR.frequency_droop)) * Input.FCR.overfulfillment;
		end
	end

% create dummy variables to transform the relative extremes to the absolute minimum and absolute maximum
	dummy_min = min(fcr_power_min,fcr_power_max);
	dummy_max = max(fcr_power_min,fcr_power_max);

% transformation of relative extremes to the absolute minimum and absolute maximum values
	fcr_power_min = dummy_min;
	fcr_power_max = dummy_max;

% clear unused variables
	clear dummy_* frequency_delta i;