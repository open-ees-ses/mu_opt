%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

% add the path of the gurobi solver, if available
	if exist('gurobi','file') == 3
		gurobipath = fileparts(which('gurobi'));
		addpath([gurobipath(1:end-6),'examples\matlab']);
	else
		warning('It is recommended to use the Gurobi solver.')
		promptMessage = sprintf('The Gurobi solver was not found in the MATLAB environment.\nDo you want to continue anyway?');
		button = questdlg(promptMessage, 'Continue without Gurobi solver', 'Continue', 'Cancel', 'Continue');
		if strcmpi(button, 'Cancel')
			error('Simulation was aborted by user.');
		end
	end

% clear unused variables
	clear gurobipath promptMessage button;

for optimization_loop = uint32(1:Input.simulation_time/Input.rolling_horizon)

%% define input profiles for optimization
% reshape power demand
	power_active_load = Input.power_active_load( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time );

% reshape power supply
	power_generation = Input.power_generation( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time );

% reshape upper and lower bounds for fcr power
	fcr_bounds = [fcr_power_min,fcr_power_max];
	fcr_bounds = fcr_bounds( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);

% reshape price signal for fcr provision
	price_fcr = Input.price_fcr( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time );

% reshape price signal of the spot market
	price_smt_low = Input.price_smt_low( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time );
	price_smt_high = Input.price_smt_high( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time );

% define initial and updated values for the loop of optimization problems
	if optimization_loop == 1
		state_of_health				= Input.ESS.state_of_health_initial;
		energy_usable				= Input.ESS.energy_nominal * state_of_health - Input.ESS.energy_nominal * (1 - Input.ESS.state_of_charge_max + Input.ESS.state_of_charge_min);
		energy_actual_initial		= energy_usable * Input.ESS.state_of_charge_initial;
		power_purchase_btm_peak_max	= Input.PS.power_threshold_initial;
	elseif Input.PS.reset_threshold && mod(double(optimization_loop-1)*Input.rolling_horizon,Input.PS.billing_period) == 0
		state_of_health				= Opt.Variables.state_of_health(end);
		energy_usable				= Input.ESS.energy_nominal * state_of_health - Input.ESS.energy_nominal * (1 - Input.ESS.state_of_charge_max + Input.ESS.state_of_charge_min);
		power_purchase_btm_peak_max	= Input.PS.power_threshold_initial;
	else
		state_of_health				= Opt.Variables.state_of_health(end);
		energy_usable				= Input.ESS.energy_nominal * state_of_health - Input.ESS.energy_nominal * (1 - Input.ESS.state_of_charge_max + Input.ESS.state_of_charge_min);
		power_purchase_btm_peak_max	= max(Input.PS.power_threshold_initial,Opt.Variables.power_purchase_btm_peak(end));
	end

%% create optimization problem
% decision variables
	bin_fcr					= optimvar('bin_fcr',					Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0,'UpperBound',1); % [1] binary variable if fcr is active
	bin_smt_purchase		= optimvar('bin_smt_purchase',			Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0,'UpperBound',1); % [1] binary variable if energy purchase at spot market is active
	bin_smt_sell			= optimvar('bin_smt_sell',				Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0,'UpperBound',1); % [1] binary variable if energy sale at spot market is active

	inverter_allocation_btm	= optimvar('inverter_allocation_btm',	Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0,'UpperBound',Input.ESS.inverter_amount); % [1] allocated inverters for btm
	inverter_allocation_ftm	= optimvar('inverter_allocation_ftm',	Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0,'UpperBound',Input.ESS.inverter_amount); % [1] allocated inverters for ftm

	energy_actual_btm		= optimvar('energy_actual_btm',			Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',energy_usable); % [kWh] actual energy content of btm partition
	energy_actual_ftm		= optimvar('energy_actual_ftm',			Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',energy_usable); % [kWh] actual energy content of ftm partition
	energy_actual_ftm_fcr	= optimvar('energy_actual_ftm_fcr',		Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',energy_usable); % [kWh] actual energy content of ftm partition during active fcr
	energy_charge_btm		= optimvar('energy_charge_btm',			Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',Input.ESS.power_active_charge*Input.sample_time); % [kWh] charging energy at btm partition
	energy_charge_ftm		= optimvar('energy_charge_ftm',			Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',Input.ESS.power_active_charge*Input.sample_time); % [kWh] charging energy at ftm partition
	energy_discharge_btm	= optimvar('energy_discharge_btm',		Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',Input.ESS.power_active_discharge*Input.sample_time); % [kWh] discharging energy at btm partition
	energy_discharge_ftm	= optimvar('energy_discharge_ftm',		Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',Input.ESS.power_active_discharge*Input.sample_time); % [kWh] discharging energy at ftm partition
	energy_ftm2btm			= optimvar('energy_ftm2btm',			Input.optimization_time/Input.sample_time,1,'LowerBound',Input.ESS.energy_shift(1),'UpperBound',Input.ESS.energy_shift(2)); % [kWh] energy exchange from ftm partition to btm partition
	energy_usable_btm		= optimvar('energy_usable_btm',			Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',energy_usable); % [kWh] usable/reserved energy content for btm partition
	energy_usable_ftm		= optimvar('energy_usable_ftm',			Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',energy_usable); % [kWh] usable/reserved energy content for btm partition
	energy_curtailment_btm	= optimvar('energy_curtailment_btm',	Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',Input.curtailment_btm_max*Input.sample_time); % [kWh] curtailment energy at btm node
	energy_curtailment_ftm	= optimvar('energy_curtailment_ftm',	Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',Input.curtailment_ftm_max*Input.sample_time); % [kWh] curtailment energy at ftm node

	power_fcr_actual		= optimvar('power_fcr_actual',			Input.optimization_time/Input.sample_time,1,'LowerBound',-min(Input.ESS.power_active_charge,Input.ESS.power_active_discharge),'UpperBound',min(Input.ESS.power_active_charge,Input.ESS.power_active_discharge)); % [kW] actual power for fcr provision (considering frequency and degrees of freedom)
	power_fcr_allocation	= optimvar('power_fcr_allocation',		Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',min(Input.ESS.power_active_charge,Input.ESS.power_active_discharge)); % [kW] allocated/reserved power for fcr provision
	power_fcr_remunerated	= optimvar('power_fcr_remunerated',		Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',min(Input.ESS.power_active_charge,Input.ESS.power_active_discharge)); % [kW] remunerated power for fcr provision (sharing revenue with energy trader)
	power_purchase_btm		= optimvar('power_purchase_btm',		Input.optimization_time/Input.sample_time,1,'LowerBound',0); % [kW] purchased power from grid at btm node
	power_purchase_btm_peak	= optimvar('power_purchase_btm_peak',	1,1,'LowerBound',power_purchase_btm_peak_max); % [kW] peak value of purchased power at btm node
	power_sell_btm			= optimvar('power_sell_btm',			Input.optimization_time/Input.sample_time,1,'LowerBound',0); % [kW] sold power to grid at btm node
	power_purchase_ftm_smt	= optimvar('power_purchase_ftm_smt',	Input.optimization_time/Input.sample_time,1,'LowerBound',0); % [kW] maximum power for energy purchase at spot market (ftm node)
	power_sell_ftm_smt		= optimvar('power_sell_ftm_smt',		Input.optimization_time/Input.sample_time,1,'LowerBound',0); % [kW] maximum power for energy sale at spot market (ftm node)

	profit_fcr				= optimvar('profit_fcr',				Input.optimization_time/Input.sample_time,1); % [EUR] profit from fcr
	profit_smt				= optimvar('profit_smt',				Input.optimization_time/Input.sample_time,1); % [EUR] profit from spot market trading
	profit_ps				= optimvar('profit_ps',					1,1); % [EUR] profit from ps
	profit_sci				= optimvar('profit_sci',				Input.optimization_time/Input.sample_time,1); % [EUR] profit from sci
	costs_cycle				= optimvar('costs_cycle',				Input.optimization_time/Input.sample_time,1); % [EUR] economic costs due to cycle degradation

% objective function with underlying profit functions
	opt_problem = optimproblem('ObjectiveSense','maximize','Objective',...
		Input.FCR.objective_weight * sum(profit_fcr) ...
		+ Input.PS.objective_weight * profit_ps ...
		+ Input.SCI.objective_weight * sum(profit_sci) ...
		+ Input.SMT.objective_weight * sum(profit_smt) ...
		- sum(costs_cycle));

% call constraints
	run('script_21_constraints.m');

%% solve optimization
% define solver options
	opt_options = optimoptions('intlinprog','Display','none','MaxTime',Input.max_optimization_time,'RelativeGapTolerance',Input.max_relative_optimization_gap);

% call solver
	[opt_variables,opt_objective,opt_flag,opt_settings] = solve(opt_problem,'Options',opt_options);

% check optimization success
	if opt_flag < 1; error('Optimization was not successful'); end

% create optimization container
	Optimization.Problem	= opt_problem;
	Optimization.Options	= opt_options;
	Optimization.Settings	= opt_settings;
	Optimization.Variables	= opt_variables;
	Optimization.Objective	= opt_objective;
	Optimization.ExitFlag	= opt_flag;

% correction of peak shaving threshold for relevant time period
	Optimization.Variables.power_purchase_btm_peak = max(power_purchase_btm_peak_max,...
		max(Optimization.Variables.power_purchase_btm(1:Input.rolling_horizon/Input.sample_time)-Optimization.Variables.power_sell_btm(1:Input.rolling_horizon/Input.sample_time)));

% save all optimization results
	if Input.optimization_full && Input.simulation_time/Input.rolling_horizon > 1
		Optimization_full(optimization_loop) = Optimization;
	end

%% clear unused variables
	clear bin_* costs_* energy_* fcr_bounds inverter_* opt_* power_* price_* profit_*;	

%% correction of decision variables (vectors with uniform dimensions)
	fields = fieldnames(Optimization.Variables);
	for i = uint32(1:length(fields))
		field = ['Optimization.Variables.',fields{i}];
		field_dummy = eval(field);
		if length(field_dummy) ~= Input.optimization_time/Input.sample_time
			eval([field,' = repelem(',field,',(Input.optimization_time/Input.sample_time)/length(field_dummy),1);']);
		end
	end
	clear field* i;

%% save only data during the rolling horizon
	fields = fieldnames(Optimization.Variables);
	for i = uint32(1:length(fields))
		Optimization.Variables.(fields{i})(Input.rolling_horizon/Input.sample_time+1:end) = [];
	end
	clear field* i;

%% calculate actual energy content and state of charge on system level
	Optimization.Variables.energy_actual = Optimization.Variables.energy_actual_btm + Optimization.Variables.energy_actual_ftm + Input.ESS.energy_nominal*Input.ESS.state_of_charge_min;
	Optimization.Variables.state_of_charge = Optimization.Variables.energy_actual ...
		./ ( Optimization.Variables.energy_usable_btm + Optimization.Variables.energy_usable_ftm + Input.ESS.energy_nominal * (1 - Input.ESS.state_of_charge_max + Input.ESS.state_of_charge_min) );

%% call script for the state of health calculation
	run('script_22_soh_calculation.m');

%% concatenate results from optimizations
	Opt.Problem							= Optimization.Problem;
	Opt.Options							= Optimization.Options;
	Opt.Settings(optimization_loop)		= Optimization.Settings;
	fields = fieldnames(Optimization.Variables);
	for i = uint32(1:length(fields))
		if optimization_loop == 1
			Opt.Variables.(fields{i})	= Optimization.Variables.(fields{i});
		else
			Opt.Variables.(fields{i})	= [Opt.Variables.(fields{i});Optimization.Variables.(fields{i})];
		end
	end
	Opt.Objective(optimization_loop)	= Optimization.Objective;
	Opt.ExitFlag(optimization_loop)		= Optimization.ExitFlag;
	clear Optimization field* i;
	
% show optimization progress	
	disp(['[ ',num2str(optimization_loop),' / ',num2str(round(Input.simulation_time/Input.rolling_horizon,0)),' ] optimizations completed',newline]);

%% save dummy file
	if mod(optimization_loop,Input.intermediate_data) == 0 && optimization_loop ~= Input.simulation_time/Input.rolling_horizon
		run('script_30_post_calculation.m');
	end
end

% clear unused variables
	clear fcr_power_max fcr_power_min charge_throughput_previous time_previous state_of_health;

%% correct profit from frequency containment reserve
	Opt.Variables.profit_fcr = cumsum(Opt.Variables.profit_fcr);

%% correct profit from peak shaving
	power_residual = Input.power_active_load(1:Input.simulation_time/Input.sample_time) + Input.ESS.support_power - Input.power_generation(1:Input.simulation_time/Input.sample_time);
	power_residual_pos = power_residual; power_residual_pos(power_residual_pos < 0) = 0;
	ps_periods = ceil(Input.simulation_time/Input.PS.billing_period);
	if ps_periods > 1
		power_residual_pos = [power_residual_pos',zeros(1,ps_periods*Input.PS.billing_period-length(power_residual_pos))];
		power_residual_pos = reshape(power_residual_pos,ps_periods,numel(power_residual_pos)/ps_periods);
		power_residual_pos = cummax(power_residual_pos,1);
		power_residual_pos = reshape(power_residual_pos,numel(power_residual_pos),1);
		power_residual_pos(length(power_residual)+1:end) = [];
	end
	Opt.Variables.profit_ps = (cummax(power_residual_pos) - Opt.Variables.power_purchase_btm_peak) * Input.PS.price;
	clear power_residual* ps_periods*;

%% correct profit from self-consumption increase
	power_residual = Input.power_active_load(1:Input.simulation_time/Input.sample_time) + Input.ESS.support_power - Input.power_generation(1:Input.simulation_time/Input.sample_time);
	power_residual_pos = power_residual; power_residual_pos(power_residual_pos < 0) = 0;
	power_residual_neg = power_residual; power_residual_neg(power_residual_neg > 0) = 0;
	power_residual_neg = -power_residual_neg; power_residual_neg(power_residual_neg > Input.SCI.feedin_limit) = Input.SCI.feedin_limit;
	profit_no_ess_sci = (Input.SCI.grid_remuneration * Input.sample_time .* power_residual_neg - Input.SCI.grid_tariff * Input.sample_time .* power_residual_pos) ...
		- Input.SCI.price_selfconsumed * Input.sample_time .* (Input.power_active_load(1:Input.simulation_time/Input.sample_time) - power_residual_pos);
	Opt.Variables.profit_sci = cumsum(Opt.Variables.profit_sci - profit_no_ess_sci);
	clear i power_residual* profit_no_ess*;

%% correct profit from spot market trading
	Opt.Variables.profit_smt = cumsum(Opt.Variables.profit_smt);

%% rename concatenated struct
	Optimization = Opt;
	clear Opt;