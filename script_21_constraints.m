%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

%% grouping of allocation switching time
% this code serves to ensure that the rolling horizon does not have to be a multiple of the allocation switching time
	period_previous = mod(double(optimization_loop-1)*Input.rolling_horizon,Input.ESS.allocation_switching_time)/Input.sample_time;
	period_start = Input.ESS.allocation_switching_time/Input.sample_time - period_previous; if period_start == Input.ESS.allocation_switching_time/Input.sample_time; period_start = 0; end; period_start = min(period_start,Input.optimization_time/Input.sample_time);
	period_normal = floor((Input.optimization_time/Input.sample_time-period_start)/(Input.ESS.allocation_switching_time/Input.sample_time))*(Input.ESS.allocation_switching_time/Input.sample_time); period_normal = max(period_normal,0);
	period_end = Input.optimization_time/Input.sample_time-period_start-period_normal;
	opt_problem.Constraints.allocation_cycle_btm = optimconstr(Input.optimization_time/Input.sample_time);
	opt_problem.Constraints.allocation_cycle_ftm = optimconstr(Input.optimization_time/Input.sample_time);
	if period_start ~= 0
		opt_problem.Constraints.allocation_cycle_btm(1:period_start) = ...
			inverter_allocation_btm(1:period_start) == repelem(Opt.Variables.inverter_allocation_btm(end),period_start,1);
		opt_problem.Constraints.allocation_cycle_ftm(1:period_start) = ...
			inverter_allocation_ftm(1:period_start) == repelem(Opt.Variables.inverter_allocation_ftm(end),period_start,1);
	end
	for i = uint32(1:period_normal*Input.sample_time/Input.ESS.allocation_switching_time)
		opt_problem.Constraints.allocation_cycle_btm(period_start+(i-1)*(Input.ESS.allocation_switching_time/Input.sample_time)+1:period_start+(i)*(Input.ESS.allocation_switching_time/Input.sample_time)) = ...
			inverter_allocation_btm(period_start+(i-1)*(Input.ESS.allocation_switching_time/Input.sample_time)+1:period_start+(i)*(Input.ESS.allocation_switching_time/Input.sample_time)) == ...
			repelem(inverter_allocation_btm(period_start+(i)*(Input.ESS.allocation_switching_time/Input.sample_time)),Input.ESS.allocation_switching_time/Input.sample_time,1);
		opt_problem.Constraints.allocation_cycle_ftm(period_start+(i-1)*(Input.ESS.allocation_switching_time/Input.sample_time)+1:period_start+(i)*(Input.ESS.allocation_switching_time/Input.sample_time)) = ...
			inverter_allocation_ftm(period_start+(i-1)*(Input.ESS.allocation_switching_time/Input.sample_time)+1:period_start+(i)*(Input.ESS.allocation_switching_time/Input.sample_time)) == ...
			repelem(inverter_allocation_ftm(period_start+(i)*(Input.ESS.allocation_switching_time/Input.sample_time)),Input.ESS.allocation_switching_time/Input.sample_time,1);
	end
	if period_end ~= 0
		opt_problem.Constraints.allocation_cycle_btm(end-period_end+1:end) = ...
			inverter_allocation_btm(end-period_end+1:end) == repelem(inverter_allocation_btm(end),period_end,1);
		opt_problem.Constraints.allocation_cycle_ftm(end-period_end+1:end) = ...
			inverter_allocation_ftm(end-period_end+1:end) == repelem(inverter_allocation_ftm(end),period_end,1);
	end
	clear period_* i;

%% grouping of fcr provision period
% this code serves to ensure that the rolling horizon does not have to be a multiple of the fcr provision time
	period_previous = mod(double(optimization_loop-1)*Input.rolling_horizon,Input.FCR.period)/Input.sample_time;
	period_start = Input.FCR.period/Input.sample_time - period_previous; if period_start == Input.FCR.period/Input.sample_time; period_start = 0; end; period_start = min(period_start,Input.optimization_time/Input.sample_time);
	period_normal = floor((Input.optimization_time/Input.sample_time-period_start)/(Input.FCR.period/Input.sample_time))*(Input.FCR.period/Input.sample_time); period_normal = max(period_normal,0);
	period_end = Input.optimization_time/Input.sample_time-period_start-period_normal;
	opt_problem.Constraints.fcr_cycle = optimconstr(Input.optimization_time/Input.sample_time);
	if period_start ~= 0
		opt_problem.Constraints.fcr_cycle(1:period_start) = ...
			power_fcr_allocation(1:period_start) == repelem(Opt.Variables.power_fcr_allocation(end),period_start,1);
	end
	for i = uint32(1:period_normal*Input.sample_time/Input.FCR.period)
		opt_problem.Constraints.fcr_cycle(period_start+(i-1)*(Input.FCR.period/Input.sample_time)+1:period_start+(i)*(Input.FCR.period/Input.sample_time)) = ...
			power_fcr_allocation(period_start+(i-1)*(Input.FCR.period/Input.sample_time)+1:period_start+(i)*(Input.FCR.period/Input.sample_time)) == repelem(power_fcr_allocation(period_start+(i)*(Input.FCR.period/Input.sample_time)),Input.FCR.period/Input.sample_time,1);
	end
	if period_end ~= 0
		opt_problem.Constraints.fcr_cycle(end-period_end+1:end) = ...
			power_fcr_allocation(end-period_end+1:end) == repelem(power_fcr_allocation(end),period_end,1);
	end
	clear period_* i;

%% grouping of spot market provision time
% this code serves to ensure that the rolling horizon does not have to be a multiple of the spot market provision period
	period_previous = mod(double(optimization_loop-1)*Input.rolling_horizon,Input.SMT.period)/Input.sample_time;
	period_start = Input.SMT.period/Input.sample_time - period_previous; if period_start == Input.SMT.period/Input.sample_time; period_start = 0; end; period_start = min(period_start,Input.optimization_time/Input.sample_time);
	period_normal = floor((Input.optimization_time/Input.sample_time-period_start)/(Input.SMT.period/Input.sample_time))*(Input.SMT.period/Input.sample_time); period_normal = max(period_normal,0);
	period_end = Input.optimization_time/Input.sample_time-period_start-period_normal;
	opt_problem.Constraints.smt_cycle_purchase = optimconstr(Input.optimization_time/Input.sample_time);
	opt_problem.Constraints.smt_cycle_sell = optimconstr(Input.optimization_time/Input.sample_time);
	if period_start ~= 0
		opt_problem.Constraints.smt_cycle_purchase(1:period_start) = ...
			power_purchase_ftm_smt(1:period_start) == repelem(Opt.Variables.power_purchase_ftm_smt(end),period_start,1);
		opt_problem.Constraints.smt_cycle_sell(1:period_start) = ...
			power_sell_ftm_smt(1:period_start) == repelem(Opt.Variables.power_sell_ftm_smt(end),period_start,1);
	end
	for i = uint32(1:period_normal*Input.sample_time/Input.SMT.period)
		opt_problem.Constraints.smt_cycle_purchase(period_start+(i-1)*(Input.SMT.period/Input.sample_time)+1:period_start+(i)*(Input.SMT.period/Input.sample_time)) = ...
			power_purchase_ftm_smt(period_start+(i-1)*(Input.SMT.period/Input.sample_time)+1:period_start+(i)*(Input.SMT.period/Input.sample_time)) == repelem(power_purchase_ftm_smt(period_start+(i)*(Input.SMT.period/Input.sample_time)),Input.SMT.period/Input.sample_time,1);
		opt_problem.Constraints.smt_cycle_sell(period_start+(i-1)*(Input.SMT.period/Input.sample_time)+1:period_start+(i)*(Input.SMT.period/Input.sample_time)) = ...
			power_sell_ftm_smt(period_start+(i-1)*(Input.SMT.period/Input.sample_time)+1:period_start+(i)*(Input.SMT.period/Input.sample_time)) == repelem(power_sell_ftm_smt(period_start+(i)*(Input.SMT.period/Input.sample_time)),Input.SMT.period/Input.sample_time,1);
	end
	if period_end ~= 0
		opt_problem.Constraints.smt_cycle_purchase(end-period_end+1:end) = ...
			power_purchase_ftm_smt(end-period_end+1:end) == repelem(power_purchase_ftm_smt(end),period_end,1);
		opt_problem.Constraints.smt_cycle_sell(end-period_end+1:end) = ...
			power_sell_ftm_smt(end-period_end+1:end) == repelem(power_sell_ftm_smt(end),period_end,1);
	end
	clear period_* i;

%% create special matrix for energy conservation
    t_minus_one_mat = sparse(2:(Input.optimization_time/Input.sample_time),1:(Input.optimization_time/Input.sample_time-1),1);
    t_minus_one_mat(Input.optimization_time/Input.sample_time,Input.optimization_time/Input.sample_time) = 0;

%% profit functions for applications
	opt_problem.Constraints.profit_fcr = ... % profit function for fcr
		profit_fcr == price_fcr * Input.sample_time .* power_fcr_remunerated;
	opt_problem.Constraints.profit_smt = ... % profit function for spot market trading
		profit_smt == (price_smt_high - Input.SMT.charges_sell) * Input.sample_time .* power_sell_ftm_smt ...
		- (price_smt_low + Input.SMT.charges_purchase) * Input.sample_time .* power_purchase_ftm_smt;
	opt_problem.Constraints.profit_ps = ... % profit function for ps
		profit_ps == Input.PS.price * (max(max(power_active_load + Input.ESS.support_power - power_generation),power_purchase_btm_peak_max) - power_purchase_btm_peak);
	opt_problem.Constraints.profit_sci = ... % profit function for sci
		profit_sci == (Input.SCI.grid_remuneration * Input.sample_time .* power_sell_btm - Input.SCI.grid_tariff * Input.sample_time .* power_purchase_btm) ...
		- (Input.SCI.price_selfconsumed * Input.sample_time .* (power_active_load + Input.ESS.support_power - power_purchase_btm));

%% economic costs due to cycle degradation of the storage system
	opt_problem.Constraints.costs_cycle = ...
		costs_cycle == (energy_charge_btm + energy_discharge_btm + energy_charge_ftm + energy_discharge_ftm) ...
		/ (2 * Input.ESS.energy_nominal) ...
		* (Input.ESS.invest_system / Input.ESS.efc_capability);

%% constraints at ess
if isfield(Input,'reserved_power_btm') && Input.reserved_power_btm ~= 'x'
	opt_problem.Constraints.allocation_power_btm = ... % allocated power for btm partition during parallel operation
		inverter_allocation_btm == round(Input.reserved_power_btm * Input.ESS.inverter_amount);
end
% ess: inverter allocation
	opt_problem.Constraints.allocation_power_btm_charge = ... % allocated charging power for btm applications
		(energy_charge_btm / Input.sample_time) <= inverter_allocation_btm * Input.ESS.power_active;
	opt_problem.Constraints.allocation_power_btm_discharge = ... % allocated discharging power for btm applications
		(energy_discharge_btm / Input.sample_time) <= inverter_allocation_btm * Input.ESS.power_active;
	opt_problem.Constraints.allocation_power_ftm_charge = ... % allocated charging power for ftm applications (actual power)
		(energy_charge_ftm / Input.sample_time) <= inverter_allocation_ftm * Input.ESS.power_active;
	opt_problem.Constraints.allocation_power_ftm_discharge = ... % allocated discharging power for ftm applications (actual power)
		(energy_discharge_ftm / Input.sample_time) <= inverter_allocation_ftm * Input.ESS.power_active;
	opt_problem.Constraints.allocation_power_ftm_fcr_reserve = ... % allocated power for ftm applications (reserved power for fcr and its reserve power)
		power_fcr_allocation * (1 + Input.FCR.reserve_power) <= inverter_allocation_ftm * Input.ESS.power_active;
	opt_problem.Constraints.allocation_power_ftm = ... % allocated power for ftm applications (reserved power for fcr provision and spot market trading)
		power_fcr_allocation + power_purchase_ftm_smt + power_sell_ftm_smt <= inverter_allocation_ftm * Input.ESS.power_active;
	opt_problem.Constraints.allocation_inverter = ... % allocate all inverters
		inverter_allocation_btm + inverter_allocation_ftm == Input.ESS.inverter_amount;

% ess: usable/reserved energy content at ess
if isfield(Input,'reserved_energy_btm') && Input.reserved_energy_btm ~= 'x'
	opt_problem.Constraints.allocation_energy_btm = ... % allocated energy for btm partition during parallel operation
		energy_usable_btm == Input.reserved_energy_btm * energy_usable;
end
	opt_problem.Constraints.energy_usable = ... % allocate usable energy
		energy_usable == energy_usable_ftm + energy_usable_btm;
	opt_problem.Constraints.energy_usable_btm = ... % usable energy content for btm partition
		energy_actual_btm <= energy_usable_btm;
	opt_problem.Constraints.energy_usable_ftm = ... % usable energy content for ftm partition
		energy_actual_ftm <= energy_usable_ftm;

% ess: energy conservation at ess considering self-discharge
	if Input.ESS.state_of_charge_min <= Input.ESS.state_of_charge_end && Input.ESS.state_of_charge_end <= Input.ESS.state_of_charge_max
		opt_problem.Constraints.energy_conservation_end = ... % energy conservation within ess at the end of optimization period
			energy_usable * Input.ESS.state_of_charge_end <= energy_actual_btm(end,:) + energy_actual_ftm(end,:);
	end

% ess: btm: energy conservation at btm partition
	opt_problem.Constraints.energy_conservation_btm = ... % energy conservation at btm partition
		energy_actual_btm == t_minus_one_mat * energy_actual_btm ...
		+ energy_charge_btm * Input.ESS.efficiency_charge ...
		- energy_discharge_btm * (1/Input.ESS.efficiency_discharge) ...
		+ energy_ftm2btm ...
		- Input.ESS.energy_self_discharge * (energy_usable_btm / energy_usable);
	if optimization_loop == 1
		opt_problem.Constraints.energy_conservation_btm(1,:) = ... % energy conservation at btm partition (considering initial energy content)
			energy_actual_btm(1,:) == energy_actual_initial - energy_actual_ftm(1,:) ...
			+ energy_charge_btm(1,:) * Input.ESS.efficiency_charge ...
			- energy_discharge_btm(1,:) * (1/Input.ESS.efficiency_discharge) ...
			+ energy_ftm2btm(1,:) ...
			- Input.ESS.energy_self_discharge * (energy_usable_btm(1,:) / energy_usable);
	else
		opt_problem.Constraints.energy_conservation_btm(1,:) = ... % energy conservation at btm partition (considering initial energy content)
			energy_actual_btm(1,:) == Opt.Variables.energy_actual_btm(end) ...
			+ energy_charge_btm(1,:) * Input.ESS.efficiency_charge ...
			- energy_discharge_btm(1,:) * (1/Input.ESS.efficiency_discharge) ...
			+ energy_ftm2btm(1,:) ...
			- Input.ESS.energy_self_discharge * (energy_usable_btm(1,:) / energy_usable);
	end

% ess: ftm: energy conservation at ftm partition
	opt_problem.Constraints.energy_conservation_ftm = ... % energy conservation at ftm partition
		energy_actual_ftm == t_minus_one_mat * energy_actual_ftm ...
		+ energy_charge_ftm * Input.ESS.efficiency_charge ...
		- energy_discharge_ftm * (1/Input.ESS.efficiency_discharge) ...
		- energy_ftm2btm ...
		- Input.ESS.energy_self_discharge * (energy_usable_ftm / energy_usable);
	if optimization_loop == 1
		opt_problem.Constraints.energy_conservation_ftm(1,:) = ... % energy conservation within at ftm partition (considering initial energy content)
			energy_actual_ftm(1,:) == energy_actual_initial - energy_actual_btm(1,:) ...
			+ energy_charge_ftm(1,:) * Input.ESS.efficiency_charge ...
			- energy_discharge_ftm(1,:) * (1/Input.ESS.efficiency_discharge) ...
			- energy_ftm2btm(1,:) ...
			- Input.ESS.energy_self_discharge * (energy_usable_ftm(1,:) / energy_usable);
	else
		opt_problem.Constraints.energy_conservation_ftm(1,:) = ... % energy conservation within at ftm partition (considering initial energy content)
			energy_actual_ftm(1,:) == Opt.Variables.energy_actual_ftm(end) ...
			+ energy_charge_ftm(1,:) * Input.ESS.efficiency_charge ...
			- energy_discharge_ftm(1,:) * (1/Input.ESS.efficiency_discharge) ...
			- energy_ftm2btm(1,:) ...
			- Input.ESS.energy_self_discharge * (energy_usable_ftm(1,:) / energy_usable);
	end

%% constraints at btm partition
% btm: power balance at btm node
	opt_problem.Constraints.btm_node = ... % power balance at btm node
		power_generation + (energy_discharge_btm / Input.sample_time) + power_purchase_btm == power_active_load + Input.ESS.support_power + (energy_charge_btm / Input.sample_time) + power_sell_btm + (energy_curtailment_btm / Input.sample_time);

% btm: ps: peak shaving threshold
	opt_problem.Constraints.power_purchase_btm_peak = ... % calculate maximum power value purchased from grid
		power_purchase_btm - power_sell_btm <= power_purchase_btm_peak;

% btm: sci: feed-in limitation
	opt_problem.Constraints.power_feedin_limit = ... % feed-in limitation for power flow from btm node to grid
		power_sell_btm <= Input.SCI.feedin_limit;

%% constraints at ftm partition
% ftm: power balance at ftm node
	opt_problem.Constraints.power_ftm = ... % actually required power for fcr and spot market trading (ftm node)
		power_fcr_actual + power_sell_ftm_smt + (energy_charge_ftm / Input.sample_time) + (energy_curtailment_ftm / Input.sample_time) == (energy_discharge_ftm / Input.sample_time) + power_purchase_ftm_smt;

%% constraint for fcr provision
% fcr: create a binary for fcr provision
	opt_problem.Constraints.bin_fcr_lb = ... % lower bound of binary fcr variable
		0 <= bin_fcr * max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge) - power_fcr_allocation;
	opt_problem.Constraints.bin_fcr_ub = ... % upper bound of binary fcr variable
		bin_fcr * max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge) - power_fcr_allocation <= max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge);

% fcr: required power for fcr provision (considering the frequency, dead band, and overfulfillment)
	opt_problem.Constraints.power_fcr_allocation_min = ... % minimum allocated power for fcr provision
		power_fcr_allocation .* fcr_bounds(:,1) <= power_fcr_actual;
	opt_problem.Constraints.power_fcr_allocation_max = ... % maximum allocated power for fcr provision
		power_fcr_actual <= power_fcr_allocation .* fcr_bounds(:,2);

% fcr: limited usable energy to fulfill reserve time criteria at fcr provision
	opt_problem.Constraints.fcr_e_min = ... % minimum energy content for reserve time criteria
		Input.FCR.reserve_time * power_fcr_allocation <= energy_actual_ftm;
	opt_problem.Constraints.fcr_e_max = ... % maximum energy content for reserve time criteria
		energy_actual_ftm <= energy_usable_ftm - Input.FCR.reserve_time * power_fcr_allocation;

% fcr: minimum power for fcr provision (to prevent small values)
	opt_problem.Constraints.power_fcr_min = ... % minimum power for fcr provision
		bin_fcr * Input.FCR.minimum_power <= power_fcr_allocation;

% fcr: energy equality at the start and end of active fcr provision period
	opt_problem.Constraints.energy_actual_ftm_fcr_lb = ... % lower bound for actual energy content during active fcr periods (linearization of product)
		energy_actual_ftm - (1 - bin_fcr) * Input.ESS.energy_nominal <= energy_actual_ftm_fcr;
	opt_problem.Constraints.energy_actual_ftm_fcr_ub1 = ... % upper bound for actual energy content during active fcr periods (linearization of product)
		energy_actual_ftm_fcr <= bin_fcr * Input.ESS.energy_nominal;
	opt_problem.Constraints.energy_actual_ftm_fcr_ub2 = ... % upper bound for actual energy content during active fcr periods (linearization of product)
		energy_actual_ftm_fcr <= energy_actual_ftm;

	period_previous = mod(double(optimization_loop-1)*Input.rolling_horizon,Input.FCR.period)/Input.sample_time;
	period_start = Input.FCR.period/Input.sample_time - period_previous; if period_start == Input.FCR.period/Input.sample_time; period_start = 0; end
	period_normal = floor((Input.optimization_time/Input.sample_time-period_start)/(Input.FCR.period/Input.sample_time))*(Input.FCR.period/Input.sample_time);
	period_end = Input.optimization_time/Input.sample_time-period_start-period_normal;
	opt_problem.Constraints.energy_actual_ftm_fcr_lb_cyc = optimconstr(period_normal*Input.sample_time/Input.FCR.period+1);
	opt_problem.Constraints.energy_actual_ftm_fcr_ub_cyc = optimconstr(period_normal*Input.sample_time/Input.FCR.period+1);
	dummy_period = 1;
	if period_start ~= 0
		opt_problem.Constraints.energy_actual_ftm_fcr_lb_cyc(1) = ...
			energy_actual_ftm_fcr(period_start) >= Opt.Variables.energy_actual_ftm_fcr(end-(Input.FCR.period/Input.sample_time)+period_start+1) - Input.FCR.energy_difference;
		opt_problem.Constraints.energy_actual_ftm_fcr_ub_cyc(1) = ...
			energy_actual_ftm_fcr(period_start) <= Opt.Variables.energy_actual_ftm_fcr(end-(Input.FCR.period/Input.sample_time)+period_start+1) + Input.FCR.energy_difference;
		dummy_period = 2;
	end
	for i = uint32(1:period_normal*Input.sample_time/Input.FCR.period)
		opt_problem.Constraints.energy_actual_ftm_fcr_lb_cyc(i+dummy_period-1) = ...
			energy_actual_ftm_fcr(period_start+(i)*(Input.FCR.period/Input.sample_time)) >= energy_actual_ftm_fcr(period_start+(i-1)*(Input.FCR.period/Input.sample_time)+1) - Input.FCR.energy_difference;
		opt_problem.Constraints.energy_actual_ftm_fcr_ub_cyc(i+dummy_period-1) = ...
			energy_actual_ftm_fcr(period_start+(i)*(Input.FCR.period/Input.sample_time)) <= energy_actual_ftm_fcr(period_start+(i-1)*(Input.FCR.period/Input.sample_time)+1) + Input.FCR.energy_difference;
	end
	clear dummy* period_* i;

% fcr: calculate remunerated power for fcr provision (sharing revenue with energy trader)
	beta_FCR_pool = ( Input.FCR.revenue_share_max * Input.FCR.revenue_share_max_power - Input.FCR.revenue_share_min * Input.FCR.minimum_power ) / ( Input.FCR.revenue_share_max_power - Input.FCR.minimum_power );
	alpha_FCR_pool = ( Input.FCR.revenue_share_min - beta_FCR_pool ) * Input.FCR.minimum_power;
	opt_problem.Constraints.power_fcr_remunerated_low = ... % follow the linear course until the threshold is reached (only during active fcr periods)
		power_fcr_remunerated <= alpha_FCR_pool + beta_FCR_pool * power_fcr_allocation - (1 - bin_fcr) * alpha_FCR_pool;
	opt_problem.Constraints.power_fcr_remunerated_low_if1 = ... % z <= M * x (remuneration only during active fcr periods)
		power_fcr_remunerated <= max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge) * bin_fcr;
	opt_problem.Constraints.power_fcr_remunerated_low_if2 = ... % z >= A - ( 1 - x ) * M (remuneration only during active fcr periods)
		power_fcr_remunerated >= alpha_FCR_pool + beta_FCR_pool * power_fcr_allocation - (1 - bin_fcr) * max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge);
	opt_problem.Constraints.power_fcr_remunerated_high = ... % remunerated power remains constant after reaching the threshold
		power_fcr_remunerated <= power_fcr_allocation * Input.FCR.revenue_share_max;
	clear alpha_FCR_pool beta_FCR_pool;

%% constraint for smt application
% smt: create a binary for spot market trading
	opt_problem.Constraints.bin_smt_purchase_active_lb = ... % lower bound of binary smt variable at electricity purchase
		0 <= bin_smt_purchase * max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge) - power_purchase_ftm_smt;
	opt_problem.Constraints.bin_smt_purchase_active_ub = ... % upper bound of binary smt variable at electricity purchase
		bin_smt_purchase * max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge) - power_purchase_ftm_smt <= max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge);
	opt_problem.Constraints.bin_smt_sell_active_lb = ... % lower bound of binary smt variable at electricity sale
		0 <= bin_smt_sell * max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge) - power_sell_ftm_smt;
	opt_problem.Constraints.bin_smt_sell_active_ub = ... % upper bound of binary smt variable at electricity sale
		bin_smt_sell * max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge) - power_sell_ftm_smt <= max(Input.ESS.power_active_charge,Input.ESS.power_active_discharge);

% smt: exclusivity of purchase and sale and minimum power consideration
	opt_problem.Constraints.smt = ... % exclusivity of purchase and sale of electricity
		bin_smt_sell + bin_smt_purchase <= 1;
	opt_problem.Constraints.bin_smt_purchase = ... % minimum power for electricity purchase at spot market
		bin_smt_purchase * Input.SMT.minimum_power <= power_purchase_ftm_smt;
	opt_problem.Constraints.bin_smt_sell = ... % minimum power for electricity sale at spot market
		bin_smt_sell * Input.SMT.minimum_power <= power_sell_ftm_smt;

%% reset decision variables if specific applications are disabled
if ~Input.fcr
	opt_problem.Constraints.disable_fcr = bin_fcr == 0;
end

if ~Input.smt && ~Input.fcr
	opt_problem.Constraints.disable_ftm_1 = energy_usable_ftm == 0;
	opt_problem.Constraints.disable_ftm_2 = inverter_allocation_ftm == 0;
	opt_problem.Constraints.disable_ftm_3 = energy_ftm2btm == 0;
end

if ~Input.ps
	opt_problem.Constraints.power_purchase_btm_peak = max(power_active_load + Input.ESS.support_power - power_generation) <= power_purchase_btm_peak;
end

if ~Input.ps && ~Input.sci
	opt_problem.Constraints.disable_btm_1 = energy_usable_btm == 0;
	opt_problem.Constraints.disable_btm_2 = inverter_allocation_btm == 0;
	opt_problem.Constraints.disable_btm_3 = energy_ftm2btm == 0;
end

clear t_minus_one_mat;