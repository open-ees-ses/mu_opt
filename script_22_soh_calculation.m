%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

%% state of charge calculation
if Input.soh_caluclation
	% calculation based on: 
	% Schmalstieg, J.; K�bitz, S.; Ecker, M.; Sauer, D.U. A holistic aging model for Li(NiMnCo)O2 based 18650 lithium-ion batteries. 
	% Journal of Power Sources 2014, 257, 325�334, doi:10.1016/j.jpowsour.2014.02.012.

% calculation of the state of charge [1]
	state_of_charge = Optimization.Variables.state_of_charge;

% calculation of cell voltage from the state of charge [V]
	voltage = -17.91 * state_of_charge.^7 + 71.29 * state_of_charge.^6 + -111.1 * state_of_charge.^5 + 81.7 * state_of_charge.^4 + -24.96 * state_of_charge.^3 + 0.003646 * state_of_charge.^2 + 1.752 * state_of_charge + 3.33;

% cumulative simulation time [d]
	time = (1+double(optimization_loop-1)*(Input.rolling_horizon/Input.sample_time):1:double(optimization_loop)*(Input.rolling_horizon/Input.sample_time))' * Input.sample_time / 24;

% average voltage [V]
	voltage_average = movmean(voltage,2);

% depth of discharge from the state of charge [1]
	depth_of_discharge = abs([0;diff(state_of_charge)]);

% charge throughput per cell [Ah]
	if optimization_loop == 1
		charge_throughput = cumsum(Input.ESS.capacity_cell_nominal * depth_of_discharge);
	else
		charge_throughput = charge_throughput_previous + cumsum(Input.ESS.capacity_cell_nominal * depth_of_discharge);
	end

	for i = uint32(1:Input.rolling_horizon/Input.sample_time)
	% alpha and beta values for capacity loss calculations
		alpha_cap = (7.543 * voltage(i) - 23.75) * 10^6 * exp(-6976 / Input.ESS.temperature_cell);
		beta_cap = 7.348e-3 * (voltage_average(i) - 3.667)^2 + 7.600e-4 + 4.081e-3 * depth_of_discharge(i);

	% capacity loss calculations caused by calendar and cycle degradation
		if i == 1 && optimization_loop == 1
			Optimization.Variables.capacity_loss_calendar(i,1) = alpha_cap * time(i)^0.75;
			Optimization.Variables.capacity_loss_cycle(i,1) = beta_cap * charge_throughput(i)^0.5;
		elseif i == 1 && optimization_loop > 1
			Optimization.Variables.capacity_loss_calendar(i,1) = alpha_cap * (time(i)^0.75 - time_previous^0.75);
			Optimization.Variables.capacity_loss_cycle(i,1) = beta_cap * (charge_throughput(i)^0.5 - charge_throughput_previous^0.5);
		else
			Optimization.Variables.capacity_loss_calendar(i,1) = alpha_cap * (time(i)^0.75 - time(i-1)^0.75);
			Optimization.Variables.capacity_loss_cycle(i,1) = beta_cap * (charge_throughput(i)^0.5 - charge_throughput(i-1)^0.5);
		end
	end
	time_previous = time(end);
	charge_throughput_previous = charge_throughput(end);
else
	% capacity loss calculations caused by calendar and cycle degradation
	Optimization.Variables.capacity_loss_calendar = zeros(Input.rolling_horizon/Input.sample_time,1);
	Optimization.Variables.capacity_loss_cycle = zeros(Input.rolling_horizon/Input.sample_time,1);
end

% clear unused variables
	clear alpha_cap beta_cap charge_throughput depth_of_discharge i state_of_charge time voltage voltage_average;

% superposition of calendar and cycle degradation
	Optimization.Variables.capacity_loss_total = Optimization.Variables.capacity_loss_calendar + Optimization.Variables.capacity_loss_cycle;

% state of health calculation
	Optimization.Variables.state_of_health = state_of_health - cumsum(Optimization.Variables.capacity_loss_total);