%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

%% save dummy file
	if optimization_loop ~= Input.simulation_time/Input.rolling_horizon
		if exist(path_results,'dir') ~= 7; mkdir(path_results); end
		if exist([path_results,Input.scenario_folder],'dir') ~= 7; mkdir([path_results,Input.scenario_folder]); end
		save([path_results,Input.scenario_folder,Input.scenario_name,'_dummy']);
		return;
	end

%% generate cell array and table from input variables
% generate cell array
	c_name = fieldnames(Input);
	c_value = struct2cell(Input);
	j = 1;
	c{j,1} = 'GENERAL'; j = j+1;
	for i = uint32(1:length(c_name))
		if length(c_value{i}) == 1 && c_name{i} ~= "ESS" ...
				 && c_name{i} ~= "FCR" && c_name{i} ~= "SMT" ...
				 && c_name{i} ~= "PS" && c_name{i} ~= "SCI"
			c{j,1} = c_name{i};
			c{j,2} = c_value{i};
			j = j+1;
		end
	end; clear c_* i;

	c{j,1} = 'ESS'; j = j+1;
	c_name = fieldnames(Input.ESS);
	c_value = struct2cell(Input.ESS);
	for i = uint32(1:length(c_name))
		if length(c_value{i}) == 1
			c{j,1} = c_name{i};
			c{j,2} = c_value{i};
			j = j+1;
		end
	end; clear c_* i;

	c{j,1} = 'FCR'; j = j+1;
	c_name = fieldnames(Input.FCR);
	c_value = struct2cell(Input.FCR);
	for i = uint32(1:length(c_name))
		if length(c_value{i}) == 1
			c{j,1} = c_name{i};
			c{j,2} = c_value{i};
			j = j+1;
		end
	end; clear c_* i;

	c{j,1} = 'SMT'; j = j+1;
	c_name = fieldnames(Input.SMT);
	c_value = struct2cell(Input.SMT);
	for i = uint32(1:length(c_name))
		if length(c_value{i}) == 1
			c{j,1} = c_name{i};
			c{j,2} = c_value{i};
			j = j+1;
		end
	end; clear c_* i;

	c{j,1} = 'PS'; j = j+1;
	c_name = fieldnames(Input.PS);
	c_value = struct2cell(Input.PS);
	for i = uint32(1:length(c_name))
		if length(c_value{i}) == 1
			c{j,1} = c_name{i};
			c{j,2} = c_value{i};
			j = j+1;
		end
	end; clear c_* i;

	c{j,1} = 'SCI'; j = j+1;
	c_name = fieldnames(Input.SCI);
	c_value = struct2cell(Input.SCI);
	for i = uint32(1:length(c_name))
		if length(c_value{i}) == 1
			c{j,1} = c_name{i};
			c{j,2} = c_value{i};
			j = j+1;
		end
	end; clear c_* i j;

% generate table from cell array
	input_table = cell2table(c);
	clear c;

%% plot data, clear unused variables and files, save output
% total runtime of the simulation
	Optimization.total_runtime = toc;
	disp([newline,'Total runtime: ',num2str(round(Optimization.total_runtime,2)),' sec.']);

% save relevant data
	if exist(path_results,'dir') ~= 7; mkdir(path_results); end
	if exist([path_results,Input.scenario_folder],'dir') ~= 7; mkdir([path_results,Input.scenario_folder]); end
	writetable(input_table,[path_results,Input.scenario_folder,Input.scenario_name,'_setting.txt'],'WriteVariableNames',false); clear input_table;
	if exist('Optimization_full','var') == 0
		save([path_results,Input.scenario_folder,Input.scenario_name,'_data'],'Input','Optimization');
	else
		save([path_results,Input.scenario_folder,Input.scenario_name,'_data'],'Input','Optimization','Optimization_full');
	end

% create and save plot
	run('script_31_plotting.m');
	print([path_results,Input.scenario_folder,Input.scenario_name,'_figure'],'-dpng','-r300');

% clear unused variables and files
	warning('off','MATLAB:DELETE:FileNotFound');
	delete('*.asv', [path_results,Input.scenario_folder,Input.scenario_name,'_dummy.mat']);
	clear optimization_loop path_results system_time fig*;
	warning('on','all');