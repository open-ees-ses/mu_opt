%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
% 2020-10-10 Stefan Englberger (author)

%%  How to cite:
% Stefan Englberger, Andreas Jossen, Holger Hesse. (2020). Unlocking the Potential of Battery 
% Storage With the Dynamic Stacking of Multiple Applications. Cell Reports Physical Science, 1(11).
% https://doi.org/10.1016/j.xcrp.2020.100238

%% plot data
	lw = 1.3;
	set(0, 'DefaultLineLineWidth', lw);
	set(0, 'DefaultFigureRenderer', 'painters');
	mymap=[0.7796,0.6021,0;0.8396,0.3018,0;0.0048,0.4473,0.7503;0,0.7282,0.7897];
% define font
	font_name = 'Palatino Linotype';
	font_size = 8;
	token_size = [15 10];
	leg_color = uint8(255*[1;1;1;.7]);
	subplot_size = [0.41, 0.285];
	subplot_x_coordinate = linspace(0.06,1-0.01-subplot_size(1),2);
	subplot_y_coordinate = linspace(0.08,1-0.005-subplot_size(2),3);

% create sampling vector for the x-axis (time in days)
	plot_sampling = Input.simulation_start/(24*1)+Input.simulation_time/(24*1)/(Input.simulation_time/Input.sample_time): ...
		Input.simulation_time/(24*1)/(Input.simulation_time/Input.sample_time): ...
		Input.simulation_start/(24*1)+Input.simulation_time/(24*1);
	xlimtime = [Input.simulation_start/(24*1) (Input.simulation_start+Input.simulation_time)/(24*1)];

% initialize figure
	fig1 = figure('Name','Plots', 'Units','centimeters', 'Position',[5 5 20 12]);

% plot btm power
	ax1 = subplot(3,2,1); hold on; box on; grid on;
		bar(plot_sampling,Input.power_active_load(1:Input.simulation_time/Input.sample_time)/1000+Input.ESS.support_power/1000-Input.power_generation(1:Input.simulation_time/Input.sample_time)/1000,1, 'FaceColor','k', 'FaceAlpha',0.5, 'EdgeColor','none', 'DisplayName','P^{BTM,w/o ESS}');
		bar(plot_sampling,Optimization.Variables.power_purchase_btm/1000-Optimization.Variables.power_sell_btm/1000,1, 'EdgeColor','none', 'Facecolor',mymap(2,:), 'Facealpha',0.7, 'DisplayName','P^{BTM, w/ ESS}');
		stairs(plot_sampling,Optimization.Variables.power_purchase_btm_peak/1000, 'Color',mymap(2,:), 'LineWidth',lw, 'DisplayName','P^{PS}');
		xlim(xlimtime); xticklabels([]);
		leg = legend('Location','South', 'Orientation','horizontal'); ylabel('Power (MW)');
		leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
		ax1.Layer = 'top';

% plot frequency and fcr power
	ax2 = subplot(3,2,3); hold on; box on; grid on;
		yyaxis left; ylabel('Frequency (Hz)'); plot(plot_sampling,Input.frequency(1:length(plot_sampling)),'Color','k', 'DisplayName','Frequency');
		yyaxis right; ylabel('Power (MW)'); plot(plot_sampling,Optimization.Variables.power_fcr_actual/1e3,'Color',mymap(3,:), 'DisplayName','FCR Power');
		xlim(xlimtime); xticklabels([]);
		ax = gca;
			ax.YAxis(1).Color = 'k';
			ax.YAxis(2).Color = mymap(3,:);
		leg = legend('Location','South', 'Orientation','horizontal');
		leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
		ax2.Layer = 'top';

% plot smt price and smt power
	ax3 = subplot(3,2,5); hold on; box on; grid on;
		x = plot_sampling;
		y1 = transpose(Input.price_smt_low(1:length(plot_sampling))*1000);
		y2 = transpose(Input.price_smt_high(1:length(plot_sampling))*1000);
		yyaxis left; ylabel('Price (EUR/MWh)');
			 p = patch([x fliplr(x)], [y1 fliplr(y2)], 'g');
			 p.FaceColor = 'k'; p.FaceAlpha = 0.3;
			 p.EdgeColor = 'k'; p.EdgeAlpha = 0.4;
			 p.LineWidth = lw;
		yyaxis right; ylabel('Power (MW)'); stairs(plot_sampling,Optimization.Variables.power_sell_ftm_smt/1e3-Optimization.Variables.power_purchase_ftm_smt/1e3,'-', 'Color',mymap(4,:), 'LineWidth',lw, 'DisplayName','SMT Power');
		xlim(xlimtime);
		ax = gca;
			ax.YAxis(1).Color = 'k';
			ax.YAxis(2).Color = mymap(4,:);
		leg = legend({'SMT Price','SMT Power'}, 'Location','South', 'Orientation','horizontal');
		leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
		ax3.Layer = 'top';
		xlabel('Time (d)');
		
% plot power allocation
	ax4 = subplot(3,2,2); hold on; box on; grid on;
		bar(plot_sampling,Optimization.Variables.inverter_allocation_ftm+Optimization.Variables.inverter_allocation_btm,1, 'FaceColor',mean(mymap(3:4,:)), 'EdgeColor','none', 'DisplayName','FTM');
		bar(plot_sampling,Optimization.Variables.inverter_allocation_btm,1, 'FaceColor',mean(mymap(1:2,:)), 'EdgeColor','none', 'DisplayName','BTM');
		xlim(xlimtime);  xticklabels([]); ylim([0 Input.ESS.inverter_amount]);
		leg = legend('Location','North', 'Orientation','horizontal'); ylabel('Number of inverters');
		leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
		ax4.Layer = 'top';	

% plot energy allocation
	ax5 = subplot(3,2,4); hold on; box on; grid on;
		p(1) = bar(plot_sampling,(Optimization.Variables.energy_usable_btm+Optimization.Variables.energy_usable_ftm)./(Optimization.Variables.energy_usable_btm+Optimization.Variables.energy_usable_ftm)*100,1, 'FaceColor',mean(mymap(3:4,:)), 'EdgeColor','none', 'DisplayName','FTM');
		p(2) = bar(plot_sampling,(Optimization.Variables.energy_usable_btm)./(Optimization.Variables.energy_usable_btm+Optimization.Variables.energy_usable_ftm)*100,1, 'FaceColor',mean(mymap(1:2,:)), 'EdgeColor','none', 'DisplayName','BTM');
		xlim(xlimtime); xticklabels([]); ylim([0 100]);
		leg = legend(p, 'Location','North', 'Orientation','horizontal'); ylabel('Reserved Energy (%)');
		leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
		ax5.Layer = 'top';

% plot state of charge
	ax6 = subplot(3,2,6); hold on; box on; grid on;
		ylabel('State of Charge (%)'); ylim([0 100]);
		bar(plot_sampling,(Optimization.Variables.energy_usable_btm+Optimization.Variables.energy_usable_ftm)./(Optimization.Variables.energy_usable_btm+Optimization.Variables.energy_usable_ftm).*Optimization.Variables.state_of_charge*100,1, 'FaceColor',mean(mymap(3:4,:)), 'EdgeColor','none', 'DisplayName','FTM');
		bar(plot_sampling,(Optimization.Variables.energy_usable_btm)./(Optimization.Variables.energy_usable_btm+Optimization.Variables.energy_usable_ftm).*Optimization.Variables.state_of_charge*100,1, 'FaceColor',mean(mymap(1:2,:)), 'EdgeColor','none', 'DisplayName','BTM');
		plot(plot_sampling,Optimization.Variables.state_of_charge*100, '-k', 'DisplayName','SOC');
		xlim(xlimtime);
		leg = legend('Location','North', 'Orientation','horizontal'); xlabel('Time (d)');
		leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
		ax6.Layer = 'top';
		
		set(ax1, 'Units', 'normalized'); set(ax1, 'Position', [subplot_x_coordinate(1), subplot_y_coordinate(3), subplot_size]);
		set(ax2, 'Units', 'normalized'); set(ax2, 'Position', [subplot_x_coordinate(1), subplot_y_coordinate(2), subplot_size]);
		set(ax3, 'Units', 'normalized'); set(ax3, 'Position', [subplot_x_coordinate(1), subplot_y_coordinate(1), subplot_size]);
		set(ax4, 'Units', 'normalized'); set(ax4, 'Position', [subplot_x_coordinate(2), subplot_y_coordinate(3), subplot_size]);
		set(ax5, 'Units', 'normalized'); set(ax5, 'Position', [subplot_x_coordinate(2), subplot_y_coordinate(2), subplot_size]);
		set(ax6, 'Units', 'normalized'); set(ax6, 'Position', [subplot_x_coordinate(2), subplot_y_coordinate(1), subplot_size]);

% link the x-axis of the subplots
	linkaxes([ax1,ax2,ax3,ax4,ax5,ax6],'x');

% reset all fonts and finish the plot
	set(findall(gcf,'-property','FontName'),'FontName',font_name);
	set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
	hold off;

% clear unused variables
	clear ans* ax* font* leg* lw mymap p plot_sampling subplot* token_size x* y*;